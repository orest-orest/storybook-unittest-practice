import React, {FC} from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from "@testing-library/user-event";
import * as stories from './Button.stories'; // import all stories from the stories file
import {composeStories} from '@storybook/testing-react';

// import DropDown from "../DropDown";


const {Primary} = composeStories(stories);

test('check button render', ()=> {
        const {getByRole} = render(<Primary/>)
        expect(getByRole("button")).toBeInTheDocument()
})

