import React, {useState} from 'react';
import './App.css';
import DropDown from "./components/DropDown/DropDown";
import ModalWindow from "./components/ModalWindow/ModalWindow";
import {Button} from "./stories/Button";


function App() {
    const [modalVisibility, setModalVisibility] = useState(true)

    const handleModalVisibility = () => {
        setModalVisibility(!modalVisibility)
    }

    return (
        <div className="App" data-testid='AppIsRun'>
            {modalVisibility ? <ModalWindow closeHandler={handleModalVisibility}
                                            controlVisible={modalVisibility}
                                            width={50}
                                            height={50}
                                            content={<DropDown title={'title'}/>}
                >
                </ModalWindow>
                :
                <Button
                    label={'Open Modal'}
                    onClick={handleModalVisibility}
                    primary={true}
                />
            }
        </div>
    );
}

export default App;
