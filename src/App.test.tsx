import React from 'react';
import App from "./App";
import { render, screen } from '@testing-library/react';

test('is all component rendered', () => {
  render(<App/>)
  const el = screen.getByTestId('AppIsRun')
  expect(el).toBeInTheDocument()
})

