import React from 'react'
import TabMenu from "./TabMenu";
import {ComponentStory, ComponentMeta} from '@storybook/react';

export default {
    title: 'TabMenu',
    component: TabMenu,
    argTypes: {
        actions: {
            onClick: 'click',
        },
    },
} as ComponentMeta<typeof TabMenu>

const Template: ComponentStory<typeof TabMenu> = (args) => <TabMenu {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    title: 'TabMenu',
    content: [{tabTitle: 'Home', tabContent: 'Home page'},
        {tabTitle: 'Second', tabContent: 'Second123'},
        {tabTitle: '123', tabContent: '123123'}]
};

export const Secondary = Template.bind({})
Secondary.args = {
    title: 'TabMenu',
    content: [{tabTitle: 'Home', tabContent: 'Home page'},
        {tabTitle: 'Second', tabContent: 'Second123'},
        {tabTitle: '123', tabContent: '123123'},
        {tabTitle: 'asd', tabContent: 'asdasd'},
        {tabTitle: 'ewq', tabContent: 'qweewq'},
        {tabTitle: '1adxc23', tabContent: '12zxcxz3'},
        {tabTitle: '1adxc253', tabContent: '12zxcxz53'},
    ]
};

// export const Basic = () => (
//     <DropDown />
// )