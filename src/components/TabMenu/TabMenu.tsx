import React, {FC, useState} from 'react'

import './tabMenu.scss'

interface TabMenuProps {
    title: string;
    content: { tabTitle: string, tabContent: string }[];
    onClick?: () => void;
}


const TabMenu: FC<TabMenuProps> = ({title, content, ...props}) => {
    const [show, setShow] = useState<string | null>(null)

    const handleTab = (tabContent: string) => {
        setShow(tabContent)
    }

    return (
        <div >
            <h3>{title}</h3>
            <div className="tab-menu">
                <ul className="tab-list">
                    {content.map((item) => {
                        return  <li className="tab-list-item" key={item.tabTitle} data-testid="tab-elements">
                            <p onClick={() => {handleTab(item.tabContent)}}
                            >{item.tabTitle}</p>
                        </li>
                    })}
                </ul>
            </div>
            {show ? <p>{show}</p> : null}
        </div>
    )
}
export default TabMenu