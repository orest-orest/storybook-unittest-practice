import React, {FC} from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from "@testing-library/user-event";
import * as stories from './TabMenu.stories'; // import all stories from the stories file
import {composeStories} from '@storybook/testing-react';

// import DropDown from "../DropDown";


const {Primary, Secondary} = composeStories(stories);

describe('chek Primary and Second type of component', ()=> {
    test('is all tab item rendered', () => {
        const {getAllByTestId} = render(<Primary/>)
        const allRenderedTabs = getAllByTestId('tab-elements').map(li => li.textContent);
        const allTabsToNeedRender = Primary.args?.content?.map(item => item.tabTitle)
        expect(allTabsToNeedRender).toEqual(allRenderedTabs)
    })

    test('is all tab item rendered(Secondary)', () => {
        const {getAllByTestId} = render(<Secondary/>)
        const allRenderedTabs = getAllByTestId('tab-elements').map(li => li.textContent);
        const allTabsToNeedRender = Secondary.args?.content?.map(item => item.tabTitle)
        expect(allTabsToNeedRender).toEqual(allRenderedTabs)
    })
})

describe('chek clicks Primary and Second type of component', ()=> {
    test('is all clicks showing right content (Primary)', () => {
        render(<Primary/>)
        Primary.args?.content?.map((item) => {
            const el = screen.getByText(item.tabTitle)
            userEvent.click(el)
            const appearContent = screen.getByText(item.tabContent)
            expect(appearContent).toBeInTheDocument();
        })
    })

    test('is all clicks showing right content (Second)', () => {
        render(<Secondary/>)
        Secondary.args?.content?.map((item) => {
            const el = screen.getByText(item.tabTitle)
            userEvent.click(el)
            const appearContent = screen.getByText(item.tabContent)
            expect(appearContent).toBeInTheDocument();
        })
    })


})
