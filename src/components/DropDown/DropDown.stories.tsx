import React from 'react'
import DropDown from './DropDown'
import { ComponentStory, ComponentMeta } from '@storybook/react';

export default {
    title: 'DropDown',
    component: DropDown
} as ComponentMeta <typeof DropDown>

const Template: ComponentStory<typeof DropDown> = (args) => <DropDown {...args} />;

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
    title: 'DropDown',
    backgroundColor: '#ccc'
};

export const Secondary = Template.bind({})
Secondary.args = {
    title: 'Secondary',
    backgroundColor: '#9badee'
};

// export const Basic = () => (
//     <DropDown />
// )