import React, {FC, useState} from 'react'
import './dropDown.css'

interface TagProps {
    title?: string;
    backgroundColor?: string;
}


const DropDown: FC<TagProps> = ({title = "My title", backgroundColor}) => {

    const [accordionVisibility , setAccordionVisibility] = useState<Boolean[]>([false, false, false])

    const handleChange = (index: number) => {
        setAccordionVisibility(prevState => {
            const newState = [...prevState]
            newState[index] = !newState[index]
            return newState
        })
    }

    return (
        <div>
            <h3>{title}</h3>
        <ul>
            {accordionVisibility.map((item, index) => {
                return <li style={{listStyleType: 'none'}} key={index}>
                    <button className="accordion" style={{backgroundColor: `${backgroundColor}`}} onClick={() => handleChange(index)}>Section {index + 1}</button>
                    {accordionVisibility[index] && <div data-testid={'visible ' + index} className='panel'>
                        <p>section {index + 1}</p>
                    </div>}
                </li>
            })}
        </ul>
        </div>
    )
}
export default DropDown