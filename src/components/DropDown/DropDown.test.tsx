import React from 'react';
import { render, screen, queryByTestId } from '@testing-library/react';
import userEvent from "@testing-library/user-event";
import DropDown from "./DropDown";

test('is all component rendered', () => {
    render(<DropDown/>)
    const el1 = screen.getByText('Section 1')
    const el2 = screen.getByText('Section 2')
    const el3 = screen.getByText('Section 3')

    expect(el1).toBeInTheDocument()
    expect(el2).toBeInTheDocument()
    expect(el3).toBeInTheDocument()
})

test('unnecessery component dont render',()=>{
    render(<DropDown/>)
    const el1 = screen.queryByTestId('visible 0')
    const el2 = screen.queryByTestId('visible 1')
    const el3 = screen.queryByTestId('visible 2')

    expect(el1).toBeNull()
    expect(el2).toBeNull()
    expect(el3).toBeNull()
})

test('elements appears after clicking', () => {
    render(<DropDown/>)

    const el1 = screen.getByText('Section 1')
    const el2 = screen.getByText('Section 2')
    const el3 = screen.getByText('Section 3')

    userEvent.click(el1);
    const appears1 = screen.getByTestId('visible 0')
    expect(appears1).toBeInTheDocument();

    userEvent.click(el1);
    const disappears1 = screen.queryByTestId('visible 0')
    expect(disappears1).toBeNull()

    userEvent.click(el2);
    const appears2 = screen.getByTestId('visible 1')
    expect(appears2).toBeInTheDocument();

    userEvent.click(el2);
    const disappears2 = screen.queryByTestId('visible 1')
    expect(disappears2).toBeNull()

    userEvent.click(el3);
    const appears3 = screen.getByTestId('visible 2')
    expect(appears3).toBeInTheDocument();

    userEvent.click(el3);
    const disappears3 = screen.queryByTestId('visible 2')
    expect(disappears3).toBeNull()
})