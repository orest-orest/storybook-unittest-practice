import React, {FC, useState} from 'react'
import ReactDOM from 'react-dom'
import styles from "./modalWindow.module.scss";
import classNames from "classnames";
import CloseButton from "./CloseButton/CloseButton";

export interface ModalWindowProps extends React.HTMLAttributes<HTMLElement> {
    variant?: "primary" | "secondary",

    closeHandler(): any,

    controlVisible: Boolean,
    content?: any,
    width: number,
    height: number,
}

const ModalWindow: FC<ModalWindowProps> = ({
                                               variant = "primary",
                                               className,
                                               content,
                                               closeHandler,
                                               controlVisible,
                                               width,
                                               height,
                                               ...props
                                           }) => {
    const cn = classNames(styles.root, className, {});

    return (
        controlVisible && ReactDOM.createPortal(<div className={cn} style={{width: `${width}%`, height: `${height}%`}}
                                                     role="dialog">
            <CloseButton closeHandler={closeHandler}/>
            {content}
        </div>, document.body)
    )
}


export default ModalWindow