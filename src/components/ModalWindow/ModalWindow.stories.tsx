import { Meta, Story } from "@storybook/react";
import ModalWindow , { ModalWindowProps } from "./ModalWindow";
import {action} from "@storybook/addon-actions";

const meta: Meta = {
    title: "ModalWindow",
    component: ModalWindow,
    args: {
        children: "???",
        controlVisible: true,
        width: 80,
        height: 80,
        closeHandler: action('close'),
    },
    argTypes: {
        variant: {
            options: ["primary", "secondary"],
            control: { type: "radio" },
        },
        controlVisible: {
            options: [true, false],
            control: {type: "boolean"}
        },
        width: {
            control: {type: "range"},
        },
        height: {
            control: {type: "range"},
        },
    },
};

const Template: Story<ModalWindowProps> = args => <ModalWindow {...args} />;

export const primary = Template.bind({});

export default meta;
