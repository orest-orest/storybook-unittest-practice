import {render, screen} from "@testing-library/react";
import React from "react";
import ModalWindow from "../ModalWindow";
import DropDown from "../../DropDown/DropDown";
import TabMenu from "../../TabMenu/TabMenu";


describe("ModalWindow with any content", () => {
    beforeEach(() => {
        render(
            <ModalWindow closeHandler={() => {
            }} width={100} height={100} controlVisible={true}/>
        )
    })


    test("Modal window is rendered", () => {
        expect(screen.getByRole("dialog")).toBeInTheDocument();
    })
});

describe('Modal window with content 1', () => {
    beforeEach(() => {
        render(
            <ModalWindow closeHandler={() => {
            }} controlVisible={true} width={100} height={100}
                         content={<DropDown title={"test"}/>}/>
        )
    })

    test("Modal window is render", () => {
        expect(screen.getByRole("dialog")).toBeInTheDocument()
    })

    test('Modal window show custom content', () => {
        expect(screen.getByText("test")).toBeInTheDocument()
    })

})

describe('ModalWindww with any conent 2', () => {
    beforeEach(() => {
        render(<ModalWindow closeHandler={() => {
            }} controlVisible={true} width={100} height={100}
                            content={<TabMenu title={"test"} content={[]}/>}/>
        )
    })

    test("Tab Menu is render", ()=>{
        expect(screen.getByText('test')).toBeInTheDocument(;;/.)
    })
})