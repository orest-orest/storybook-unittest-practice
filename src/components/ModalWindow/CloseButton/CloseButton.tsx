import React, {FC, useState} from 'react'
import styles from "./CloseButton.module.scss";
import classNames from "classnames";




export interface CloseButtonProps extends React.HTMLAttributes<HTMLButtonElement>{
    closeHandler(): any,
}



const CloseButton: FC<CloseButtonProps> = ({closeHandler, className, ...props}) => {
    const cn = classNames(styles.root, className, {
    });
    return (<button className={cn} onClick={() => closeHandler()}>&times;</button>)


}


export default CloseButton