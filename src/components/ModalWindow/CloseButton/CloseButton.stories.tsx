import { Meta, Story } from "@storybook/react";
import CloseButton , { CloseButtonProps } from "./CloseButton";
import {action} from "@storybook/addon-actions";

const meta: Meta = {
    title: "ModalWindow/CloseButton",
    component: CloseButton,
    args: {
        closeHandler: action('Close')
    },
    argTypes: {
    },
};

const Template: Story<CloseButtonProps> = args => <CloseButton {...args} />;

export const primary = Template.bind({});

export default meta;
